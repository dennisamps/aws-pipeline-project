
#Application Load Balancer
resource "aws_lb" "main_lb" {
  name               = "Main"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [data.aws_security_group.ALB-SG.id]
  subnets            = [data.aws_subnet.subnets_public_1.id, data.aws_subnet.subnets_public_2.id]
}

output "lb_output" {
  value = aws_lb.main_lb.dns_name
}

#ALB Target Group
resource "aws_lb_target_group" "ECS_TG" {
  name        = "ECS"
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.vpc.id

  health_check {
    path    = "/"
    matcher = "200-302"

  }
}

#ALB Listener

resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = aws_lb.main_lb.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.ECS_TG.id
    type             = "forward"
  }
}