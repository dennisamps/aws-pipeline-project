data "aws_vpc" "vpc" {
  filter {
    name   = "cidr"
    values = ["172.31.0.0/16"]
  }
}


#Public Subnets
data "aws_subnet" "subnets_public_1" {
  filter {
    name   = "tag:Name"
    values = ["AZ 1 public 1"]
  }
}

data "aws_subnet" "subnets_public_2" {
  filter {
    name   = "tag:Name"
    values = ["AZ 2 public 1"]
  }
}

#Private Subnets
data "aws_subnet" "private_subnets_1" {
  filter {
    name   = "tag:Name"
    values = ["AZ 1 private 1"]
  }
}

data "aws_subnet" "private_subnets_2" {
  filter {
    name   = "tag:Name"
    values = ["AZ 2 private 1"]
  }
}

#Secuirty Group ALB
data "aws_security_group" "ALB-SG" {
  filter {
    name = "tag:Name"
    values = ["ALB-SG"]
  }
}

#Secuirty Group ECS
data "aws_security_group" "ECS-SG" {
  filter {
    name = "tag:Name"
    values = ["ECS-SG"]
  }
}

#Database
data "aws_db_instance" "database" {
  db_instance_identifier="wordpress-database"
}

#efs
data "aws_efs_file_system" "efs" {
  creation_token = "wp-content"
}