
#ECS Task Definition
resource "aws_ecs_task_definition" "wordpress" {
  family                = "wordpress"
  # container_definitions = file("task-definitions/wordpress-definition.json")
  container_definitions = <<EOF
  [
    {
      "memory": 300,
      "image": "wordpress:latest",
      "name": "wordpress",
      "portMappings": [
        {
          "containerPort": 80,
          "hostPort": 0
        }
      ],

      "environment": [
        {
          "name": "WORDPRESS_DB_HOST",
          "value": "${data.aws_db_instance.database.endpoint}"
        },
        {
          "name": "WORDPRESS_DB_NAME",
          "value": "wordpress"
        },
        {
          "name": "WORDPRESS_DB_PASSWORD",
          "value": "Password123"
        },
        {
          "name": "WORDPRESS_DB_USER",
          "value": "admin"
        }
      ],

      "mountPoints": [
        {
          "containerPath": "/var/www/html/wp-content",
          "sourceVolume": "wp-content"
        }
      ]
    }
  ]
  EOF

  volume {
    name = "wp-content"
    efs_volume_configuration {
      file_system_id = data.aws_efs_file_system.efs.id
      root_directory = "/"
    }
  }

}


# ECS Cluster
resource "aws_ecs_cluster" "web_server" {
  name = "web-server"
}

resource "aws_autoscaling_group" "web_server" {
  name = "ECS ASG"
  min_size = "1"
  max_size = "2"
  desired_capacity = "2"
  launch_configuration = aws_launch_configuration.web_server_lc.id
  vpc_zone_identifier = [data.aws_subnet.private_subnets_1.id, data.aws_subnet.private_subnets_2.id]
  target_group_arns     = [aws_lb_target_group.ECS_TG.arn]
}

resource "aws_launch_configuration" "web_server_lc" {
  name = "web server lc"
  image_id = "ami-0128839b21d19300e"
  instance_type = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.ecs_service_role.name
  security_groups             = [data.aws_security_group.ECS-SG.id]
  key_name = "ecs-key"
  # associate_public_ip_address = true
  user_data                   = <<EOF
#! /bin/bash
sudo apt-get update
sudo echo "ECS_CLUSTER=${aws_ecs_cluster.web_server.name}" >> /etc/ecs/ecs.config

EOF
  



}


#ECS Service
resource "aws_ecs_service" "web_server" {
  name            = "web_server"
  cluster         = aws_ecs_cluster.web_server.id
  task_definition = aws_ecs_task_definition.wordpress.id
  desired_count   = 4
  launch_type = "EC2"
  depends_on = [aws_lb_listener.http_listener]


  load_balancer {
    target_group_arn = aws_lb_target_group.ECS_TG.arn
    container_name   = "wordpress"
    container_port   = 80
  }
}



#IAM

resource "aws_iam_role" "ecs-instance-role" {
  name = "ecs-instance-role-test-web"
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["ec2.amazonaws.com"]
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs-instance-role-attachment" {
  role       = aws_iam_role.ecs-instance-role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs_service_role" {
  role = aws_iam_role.ecs-instance-role.name
}


#keygen 
resource "aws_key_pair" "ECS_key" {
  key_name   = "ecs-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCoNBX6hJ7guF5bAqec4zc6oUTwBPSaDpXOcggQFBS6vVlhvvHrZ4C9X+H9AdcYBNFJ+COD5rXh/IaS/ZtB+ZmVd7HxsKHuoSlqSAPNPuC0rQ7WvhVbHSWUPPBHPoRgYEonR+sf6MA9m/XMrRf/Dp2eTerYmncHVTF1YOFtPN0eJxOkH8QCRJbUe3/PDlJv/X8976veLMsTWfUIP9iEJzN7MxPm2TRkzDmvGxNam9pzY/kdC/noSADY0JeY+16ihgakDBlQghPZAC8jZ4bXA24si7HVQPsSJcojnZDm/AzvjKKSoDfT/2fzDe9zJg6qahfd62STi8GSMmuPajp5TTUzUgqzm5OoEsLQKBGPX+ZpKpgPtYpbFZGecCkNNbpKvNaCm0aeXL6AixjDXwNONwi3ZZhibp2T6JBKO/jWpwqGVBPQ4DBH9QcLr7nq2tvpu/yaQqtycATIntDm35ubg5+z4xeyEYDqIBNduzHsyHaDoZkPt6Z2OIFVdeRJqqrYia8= dampontuah@zaizi.com@ZZ048"
}
