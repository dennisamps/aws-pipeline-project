#subnets
data "aws_subnet" "subnets_public_1" {
  filter {
    name   = "tag:Name"
    values = ["AZ 1 public 1"]
  }
}

data "aws_subnet" "subnets_public_2" {
  filter {
    name   = "tag:Name"
    values = ["AZ 2 public 1"]
  }
}

data "aws_subnet" "subnets_private_1" {
  filter {
    name   = "tag:Name"
    values = ["AZ 1 private 1"]
  }
}

data "aws_subnet" "subnets_private_2" {
  filter {
    name   = "tag:Name"
    values = ["AZ 2 private 1"]
  }
}

data "aws_subnet" "database_subnet_1" {
  filter {
    name   = "tag:Name"
    values = ["AZ 1 private 2 database"]
  }
}

data "aws_subnet" "database_subnet_2" {
  filter {
    name   = "tag:Name"
    values = ["AZ 2 private 2 database"]
  }
}



#security groups
data "aws_security_group" "DB-SG" {
  filter {
    name = "tag:Name"
    values = ["DB SG"]
  }
}

data "aws_security_group" "ECS-SG" {
  filter {
    name = "tag:Name"
    values = ["ECS-SG"]
  }
}
