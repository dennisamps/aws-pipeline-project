#Secuirty Groups
resource "aws_security_group" "EFS_SG" {
    name = "EFS-SG"
    description = "EFS security groups"

    ingress {
        description = "Allows efs traffic from "
        from_port   = 2049
        to_port     = 2049
        protocol    = "tcp"
        security_groups = [data.aws_security_group.ECS-SG.id]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]

    }
    

    tags = {
        Name = "EFS-SG"
    }

}


#EFS
resource "aws_efs_file_system" "efs" {
  creation_token = "wp-content"
  tags = {
    Name = "wp-content"
}

}
resource "aws_efs_mount_target" "efs_private_1" {
  file_system_id = aws_efs_file_system.efs.id
  subnet_id      = data.aws_subnet.subnets_private_1.id
  security_groups= [aws_security_group.EFS_SG.id]
}

resource "aws_efs_mount_target" "efs_private_2" {
  file_system_id = aws_efs_file_system.efs.id
  subnet_id      = data.aws_subnet.subnets_private_2.id
  security_groups= [aws_security_group.EFS_SG.id]
}


