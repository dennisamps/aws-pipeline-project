


resource "aws_db_subnet_group" "database" {
  name       = "main"
  subnet_ids = [data.aws_subnet.database_subnet_1.id, data.aws_subnet.database_subnet_2.id]

  tags = {
    Name = "My DB subnet group"
  }
}



#RDS Database
resource "aws_db_instance" "wordpress_database" {
  identifier = "wordpress-database"
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "wordpress"
  username             = "admin"
  password             = "Password123"
  parameter_group_name = "default.mysql5.7"
  availability_zone    = "us-east-1a"
  db_subnet_group_name = aws_db_subnet_group.database.id
  port = "3306"
#   security_group_names = [data.aws_security_group.DB-SG.name]
  vpc_security_group_ids = [data.aws_security_group.DB-SG.id]

  tags  = {
    Name = "Database"
  }
}