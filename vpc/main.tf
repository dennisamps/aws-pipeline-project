terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# variable "vpc_id" {}

# data "aws_vpc" "vpc" {
#   id = var.vpc_id
# }

data "aws_vpc" "vpc" {
  filter {
    name = "cidr"
    values = ["172.31.0.0/16"]
  }
}


# variable "vpc_id" {
#   type = string
#   default = "vpc-03c2af32ac4b0a3a8"
# }

# Configure the AWS Provider
provider "aws" {
  region = var.region
  profile = var.profile 
  
}


#Internet Gatweway
resource "aws_internet_gateway" "gw" {
  vpc_id = data.aws_vpc.vpc.id


  tags = {
    Name = "new-IG"
  }
}


#Subnets
resource "aws_subnet" "AZ_1_public_1" {
  vpc_id     = data.aws_vpc.vpc.id
  cidr_block = "172.31.1.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "AZ 1 public 1"
    AZ = "1"
    type = "public"
  }
}

resource "aws_subnet" "AZ_1_private_1" {
  vpc_id                  = data.aws_vpc.vpc.id
  cidr_block              = "172.31.2.0/24"
  availability_zone       = "us-east-1a"

  tags = {
    Name = "AZ 1 private 1"
    AZ = "1"
    type = "private"
  }
}

resource "aws_subnet" "AZ_2_public_1" {
  vpc_id                  = data.aws_vpc.vpc.id
  cidr_block              = "172.31.3.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "AZ 2 public 1"
    AZ = "2"
    type = "public"
  }
}

resource "aws_subnet" "AZ_2_private_1" {
  vpc_id                  = data.aws_vpc.vpc.id
  cidr_block              = "172.31.4.0/24"
  availability_zone       = "us-east-1b"

  tags = {
    Name = "AZ 2 private 1"
    AZ = "2"
    type = "private"
  }
}

resource "aws_subnet" "AZ_1_private_2_database" {
  vpc_id     = data.aws_vpc.vpc.id
  cidr_block = "172.31.5.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "AZ 1 private 2 database"
    AZ = "1"
    type = "private database"
  }
}

resource "aws_subnet" "AZ_2_private_2_database" {
  vpc_id     = data.aws_vpc.vpc.id
  cidr_block = "172.31.6.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "AZ 2 private 2 database"
    AZ = "2"
    type = "private database"
  }
}


#NAT Gateways
resource "aws_nat_gateway" "public_1_ngw" {
  allocation_id = aws_eip.nat_gateway_1.id
  subnet_id     = aws_subnet.AZ_1_public_1.id

  tags = {
    Name = "gw NAT 1"
  }
}

resource "aws_nat_gateway" "public_2_ngw" {
  allocation_id = aws_eip.nat_gateway_2.id
  subnet_id     = aws_subnet.AZ_2_public_1.id

  tags = {
    Name = "gw NAT 2"
  }
}


#Elastic IPs
resource "aws_eip" "nat_gateway_1" {
  vpc      = true
}

resource "aws_eip" "nat_gateway_2" {
  vpc      = true
}


#Route Table
resource "aws_route_table" "public_routes" {
  vpc_id = data.aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }


  tags = {
    Name = "Public RT"
  }
}

resource "aws_route_table" "private_routes_AZ1" {
  vpc_id = data.aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.public_1_ngw.id
  }


  tags = {
    Name = "Private RT AZ1"
  }
}

resource "aws_route_table" "private_routes_AZ2" {
  vpc_id = data.aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.public_2_ngw.id
  }


  tags = {
    Name = "Private RT AZ2"
  }
}

# Route Table Associations
resource "aws_route_table_association" "Public_1" {
  subnet_id      = aws_subnet.AZ_1_public_1.id
  route_table_id = aws_route_table.public_routes.id
}

resource "aws_route_table_association" "Private_1" {
  subnet_id      = aws_subnet.AZ_1_private_1.id
  route_table_id = aws_route_table.private_routes_AZ1.id
}

resource "aws_route_table_association" "Public_2" {
  subnet_id      = aws_subnet.AZ_2_public_1.id
  route_table_id = aws_route_table.public_routes.id
}

resource "aws_route_table_association" "Private_2" {
  subnet_id      = aws_subnet.AZ_2_private_1.id
  route_table_id = aws_route_table.private_routes_AZ2.id
}

#Secuirty Groups
resource "aws_security_group" "ALB_SG" {
    name = "ALB-SG"
    description = "ALB security groups"

    ingress {
        description = "HTTP Traffic"
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]

    }
    

    tags = {
        Name = "ALB-SG"
    }

}

resource "aws_security_group" "ECS_SG" {
    name = "ECS-SG"
    description = "ECS security groups"

    ingress {
        description = "HTTP Traffic"
        from_port   = 0
        to_port     = 65535
        protocol    = "tcp"
        security_groups = [aws_security_group.ALB_SG.id]
    }

    ingress {
        description = "SSH Traffic"
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

        egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]

    }
    

    tags = {
        Name = "ECS-SG"
    }

}

resource "aws_security_group" "DB_SG" {
    name = "Database SG"
    description = "Database Security Groups"

    ingress {
        description = "Traffic from ECS"
        from_port   = 3306
        to_port     = 3306
        protocol    = "tcp"
        security_groups = [aws_security_group.ECS_SG.id]
    }


    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]

    }
    

    tags = {
        Name = "DB SG"
    }

}







